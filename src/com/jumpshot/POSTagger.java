package com.jumpshot;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.io.IOException;

public class POSTagger {
    /* Stanford NLP tagging API [stanford-postagger.jar] */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MaxentTagger tagger = new MaxentTagger("bidirectional-distsim-wsj-0-18.tagger");
        String sample = "This is a news website";
        String tagged = tagger.tagString(sample);
        System.out.println(tagged);
    }
}